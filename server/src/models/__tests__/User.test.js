const User = require('../User');

describe('User', () => {
  let MockTodo;
  let user;

  beforeEach(() => {
    user = new User({
      id: 1,
      name: 'John Doe',
      email: 'john@doe.com',
    });

    // eslint-disable-next-line
    MockTodo = require('mocks/todo');
  });

  it('getters should return correct information from constructor', () => {
    expect(user.getId()).toBe(1);
    expect(user.getName()).toBe('John Doe');
    expect(user.getEmail()).toBe('john@doe.com');
    expect(user.getToken()).toBe(null);
  });

  it('should set token property', () => {
    user.setToken(1);

    expect(user.getToken()).toBe(1);
  });

  it('should return todos from user', () => {
    const calculedFromUser = MockTodo.filter(t => t.userId === user.id);
    const returnedFromuser = user.getTodos();

    let invalid = false;

    calculedFromUser.every((todo, index) => {
      if (JSON.stringify(calculedFromUser[index]) !== JSON.stringify(returnedFromuser[index])) {
        invalid = true;
        return false;
      }

      return true;
    });

    expect(invalid).toBeFalsy();
  });

  it('should add todo', () => {
    const title = 'Test';
    const description = 'Test Test Test.';

    const beforeInsertTodos = user.getTodos();
    const nextInsertId = MockTodo.length;

    user.addTodo(title, description);

    const afterInsertTodos = user.getTodos();

    expect(afterInsertTodos.length).toBe(beforeInsertTodos.length + 1);

    const lastInsertedTodo = afterInsertTodos.pop();

    expect(lastInsertedTodo.id).toBe(nextInsertId + 1);
    expect(lastInsertedTodo.userId).toBe(user.getId());
    expect(lastInsertedTodo.title).toBe(title);
    expect(lastInsertedTodo.description).toBe(description);
    expect(lastInsertedTodo.finished).toBeFalsy();
  });

  it('should delete todo from user', () => {
    const id = 1;

    const beforeDeleteTodos = user.getTodos();
    const toDeleteTodo = beforeDeleteTodos.filter(t => t.id === id);

    const deletedTodo = user.deleteTodo(id);

    expect(JSON.stringify(toDeleteTodo)).toBe(JSON.stringify(deletedTodo));
    expect(user.getTodos().indexOf(toDeleteTodo)).toBe(-1);
  });

  it('should not delete todo from other user', () => {
    const id = 2;
    const deletedTodo = user.deleteTodo(id);

    expect(deletedTodo).toBeFalsy();
  });

  it('should update todo from user', () => {
    const todo = {
      id: 3,
      userId: 1,
      title: 'Test update',
      description: 'Test Test Test',
      finished: true,
    };

    const beforeUpdateTodos = user.getTodos();
    const toUpdateTodoIndex = beforeUpdateTodos
                                .indexOf(beforeUpdateTodos.find(t => t.id === todo.id));

    user.updateTodo(todo);

    const afterUpdateTodos = user.getTodos();

    const updatedTodo = afterUpdateTodos[toUpdateTodoIndex];

    expect(JSON.stringify(updatedTodo)).toBe(JSON.stringify(todo));
  });

  it('should not update todo from other user', () => {
    const todo = {
      id: 2,
      userId: 2,
      title: 'Test update',
      description: 'Test Test Test',
      finished: true,
    };

    expect(user.updateTodo(todo)).toBeFalsy();
  });
});

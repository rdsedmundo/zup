const MockTodo = require('mocks/todo');

let AUTO_INCREMENT_TODO = MockTodo.length;

class User {
  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.email = data.email;
    this.token = null;
  }

  addTodo(title, description) {
    const todo = {
      id: AUTO_INCREMENT_TODO += 1,
      userId: this.id,
      title,
      description,
      finished: false,
    };

    MockTodo.push(todo);

    return todo;
  }

  // eslint-disable-next-line
  deleteTodo(id) {
    const index = MockTodo
                    .indexOf(MockTodo.find(t => t.userId === this.id && t.id === id));

    if (index === -1) return false;

    return MockTodo.splice(index, 1);
  }

  // eslint-disable-next-line
  updateTodo(todo) {
    const index = MockTodo
                    .indexOf(MockTodo.find(t => t.userId === this.id && t.id === todo.id));

    if (index === -1) {
      return false;
    }

    const oldTodo = MockTodo[index];

    return MockTodo.splice(index, 1, {
      id: todo.id,
      userId: this.id,
      title: todo.title || oldTodo.title,
      description: todo.description || oldTodo.description,
      finished: todo.finished,
    });
  }

  getId() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  getEmail() {
    return this.email;
  }

  getToken() {
    return this.token;
  }

  getTodos() {
    return MockTodo.filter(todo => todo.userId === this.id);
  }

  setToken(token) {
    this.token = token;
  }
}

module.exports = User;

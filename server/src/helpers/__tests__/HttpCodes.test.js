const HttpCodes = require('../HttpCodes');

describe('HttpCodes', () => {
  it('HttpCodes should be valid', () => {
    expect(HttpCodes.OK).toBe(200);
    expect(HttpCodes.BAD_REQUEST).toBe(400);
    expect(HttpCodes.FORBIDDEN).toBe(403);
  });
});

const md5 = require('md5');
const User = require('models/User');

class Session {
  constructor() {
    this.users = new Map();
  }

  getUsers() {
    return this.users;
  }

  addUser(userData) {
    const user = new User(userData);

    const token = Session.generateUserToken(user);
    user.setToken(token);

    this.users.set(token, user);

    return user;
  }

  getUserByToken(token) {
    return this.users.get(token);
  }

  removeToken(token) {
    return this.users.delete(token);
  }

  static generateUserToken(user) {
    const payload = `${user.email}__${user.password}__${Math.random()}`;
    return md5(payload);
  }
}

module.exports = new Session();

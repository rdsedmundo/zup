const Session = require('../Session');

describe('Session', () => {
  let userData;

  beforeEach(() => {
    userData = {
      id: 1,
      name: 'John Doe',
      email: 'john@doe.com',
    };
  });

  it('should add user session', () => {
    const user = Session.addUser(userData);

    const userToken = user.getToken();

    expect(userToken.length).not.toBe(0);

    const userInSession = Session.getUserByToken(userToken);

    expect(JSON.stringify(userInSession)).toBe(JSON.stringify(user));
  });

  it('should delete user session', () => {
    const token = Session.getUsers().keys().next().value;

    Session.removeToken(token);

    expect(Session.getUsers().has(token)).toBeFalsy();
  });
});

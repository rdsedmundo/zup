const MockTodo = require('../todo');

describe('Todo', () => {
  it('todos should have valid schema', () => {
    function validSchema(todo) {
      const schema = ['id', 'userId', 'title', 'description', 'finished'];

      const properties = Object.getOwnPropertyNames(todo);

      if (schema.length !== properties.length) return false;

      let valid = true;

      properties.every((property) => {
        if (schema.indexOf(property) === -1) {
          valid = false;
          return false;
        }

        return true;
      });

      if (!valid) return false;

      schema.every((key) => {
        if (properties.indexOf(key) === -1) {
          valid = true;
          return false;
        }

        return true;
      });

      return valid;
    }

    MockTodo.forEach(todo => expect(validSchema(todo)).toBeTruthy());
  });
});

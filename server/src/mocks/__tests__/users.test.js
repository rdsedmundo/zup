const MockUsers = require('../users');

describe('Users', () => {
  it('users should have valid schema', () => {
    function validSchema(todo) {
      const schema = ['id', 'name', 'email', 'password'];

      const properties = Object.getOwnPropertyNames(todo);

      if (schema.length !== properties.length) return false;

      let valid = true;

      properties.every((property) => {
        if (schema.indexOf(property) === -1) {
          valid = false;
          return false;
        }

        return true;
      });

      if (!valid) return false;

      schema.every((key) => {
        if (properties.indexOf(key) === -1) {
          valid = true;
          return false;
        }

        return true;
      });

      return valid;
    }

    MockUsers.forEach(todo => expect(validSchema(todo)).toBeTruthy());
  });
});

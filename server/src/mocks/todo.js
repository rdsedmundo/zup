module.exports = [
  {
    id: 1,
    userId: 1,
    title: 'Pay bills',
    description: 'Internet, retirement plan, grocery',
    finished: false,
  },
  {
    id: 2,
    userId: 2,
    title: 'Learn Fingerstyle',
    description: 'See jwcfree youtube channel.',
    finished: false,
  },
  {
    id: 3,
    userId: 1,
    title: 'Study for the next exams',
    description: 'Linear Algebra exam will be in the next week. Math.',
    finished: true,
  },
  {
    id: 4,
    userId: 2,
    title: 'Lorem Ipsum',
    description: 'Lorem Ipsum Dolor Sit Amet',
    finished: false,
  },
  {
    id: 5,
    userId: 1,
    title: 'Donec Odio',
    description: 'Amet Dolor Ipsum',
    finished: false,
  },
];

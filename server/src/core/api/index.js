const EndpointLogin = require('./login');
const EndpointLogout = require('./logout');
const EndpointTodo = require('./todo');

class API {
  setHttpServer(httpServer) {
    this.app = httpServer;

    EndpointLogin(this.app);
    EndpointLogout(this.app);
    EndpointTodo(this.app);
  }
}

module.exports = new API();

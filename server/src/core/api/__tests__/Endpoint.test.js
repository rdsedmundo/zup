const Endpoint = require('../Endpoint');

describe('Endpoint', () => {
  it('should throw on instance abstract class', () => {
    expect(() => {
      new Endpoint();
    }).toThrowError('Endpoint is an abstract class.');
  });
});

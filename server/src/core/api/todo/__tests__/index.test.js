const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');

const API = require('core/api');
const HttpCodes = require('helpers/HttpCodes');
const Session = require('services/Session');
const User = require('models/User');

const app = express();
app.use(bodyParser.json());

API.setHttpServer(app);

describe('LogoutController', () => {
  let user;

  beforeEach(() => {
    user = new User({
      id: 1,
      name: 'John Doe',
      email: 'john@doe.com',
    });

    Session.getUsers().set('mock_token', user);
    user.setToken('mock_token');
  });

  it('should get todos', (done) => {
    request(app)
      .get('/todo')
      .set('Authorization', 'Zup mock_token')
      .expect(HttpCodes.OK)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });

  it('should add todo', (done) => {
    request(app)
      .post('/todo')
      .set('Authorization', 'Zup mock_token')
      .send({
        title: 'Testing',
        description: 'Test Test',
      })
      .expect(HttpCodes.OK)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });

  it('should update todo', (done) => {
    request(app)
      .put('/todo')
      .set('Authorization', 'Zup mock_token')
      .send({
        id: 1,
        userId: 1,
        title: 'Pay bills',
        description: 'Internet, retirement plan, grocery',
        finished: true,
      })
      .expect(HttpCodes.OK)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });


  it('should not update invalid todo', (done) => {
    request(app)
      .put('/todo')
      .set('Authorization', 'Zup mock_token')
      .send({
        id: -1,
        userId: 1,
        title: 'Pay bills',
        description: 'Internet, retirement plan, grocery',
        finished: true,
      })
      .expect(HttpCodes.BAD_REQUEST)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });

  it('should delete todo', (done) => {
    request(app)
      .delete('/todo')
      .set('Authorization', 'Zup mock_token')
      .query({ id: 1 })
      .expect(HttpCodes.OK)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });

  it('should not delete invalid todo', (done) => {
    request(app)
      .delete('/todo')
      .set('Authorization', 'Zup mock_token')
      .query({ id: -1 })
      .expect(HttpCodes.BAD_REQUEST)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });
});

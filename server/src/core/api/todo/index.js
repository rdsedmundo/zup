const Endpoint = require('../Endpoint');
const HttpCodes = require('helpers/HttpCodes');

class TodoController extends Endpoint {
  registerRoutes() {
    this.app.all('/todo', Endpoint.checkAuth);
    this.app.delete('/todo', TodoController.delete);
    this.app.get('/todo', TodoController.get);
    this.app.post('/todo', TodoController.post);
    this.app.put('/todo', TodoController.put);
  }

  static delete(request, response, next) {
    const todo = response.locals.user.deleteTodo(parseInt(request.query.id, 10));

    if (!todo) {
      return Endpoint.error(response, {
        status: HttpCodes.BAD_REQUEST,
        message: 'Supplied todo not found, or user didn\'t have authorization.',
      });
    }

    Endpoint.success(response, {
      message: 'Todo successful deleted.',
    });

    return next();
  }

  static get(request, response, next) {
    Endpoint.success(response, {
      data: response.locals.user.getTodos(),
    });

    next();
  }

  static post(request, response, next) {
    const todo = response.locals.user.addTodo(request.body.title, request.body.description);

    Endpoint.success(response, {
      message: 'Todo successful added.',
      data: todo,
    });

    next();
  }

  static put(request, response, next) {
    const todo = response.locals.user.updateTodo(request.body);

    if (!todo) {
      return Endpoint.error(response, {
        status: HttpCodes.BAD_REQUEST,
        message: 'Supplied todo not found, or user didn\'t have authorization.',
      });
    }

    Endpoint.success(response, {
      message: 'Todo successful updated.',
      data: request.body,
    });

    return next();
  }
}

module.exports = httpServer => (new TodoController(httpServer));

const HttpCodes = require('helpers/HttpCodes');
const Session = require('services/Session');

class Endpoint {
  constructor(httpServer) {
    if (new.target === Endpoint) throw new Error('Endpoint is an abstract class.');
    if (!httpServer) throw new TypeError('Endpoint requires an valid httpServer.');

    this.app = httpServer;
    this.registerRoutes();
  }

  /**
   * @description Register endpoint routes, should be overwritten
   */
  // eslint-disable-next-line
  registerRoutes() {}

  static checkAuth(request, response, next) {
    // eslint-disable-next-line
    response.locals.user = {};

    if (!request.headers.authorization) return Endpoint.raiseForbidden(request, response);

    let suppliedToken = request.headers.authorization
      .split('Zup')[1];

    if (!suppliedToken) return Endpoint.raiseForbidden(request, response);

    suppliedToken = suppliedToken.trim();

    const user = Session.getUserByToken(suppliedToken);

    if (!user) return Endpoint.raiseForbidden(request, response);

    // eslint-disable-next-line
    response.locals.user = user;

    return next();
  }

  static dispatch(type, response, payload) {
    if (!response) return;

    const sendData = {
      status: type,
    };

    if (payload.message) sendData.message = payload.message;
    if (payload.data) sendData.data = payload.data;

    response.status(payload.status || HttpCodes.OK);
    response.send(sendData);
  }

  static success(response, payload) {
    Endpoint.dispatch('success', response, payload);
  }

  static error(response, payload) {
    Endpoint.dispatch('error', response, payload);
  }

  static raiseForbidden(request, response) {
    return Endpoint.error(response, {
      status: 403,
      message: 'Unauthorized.',
    });
  }
}

module.exports = Endpoint;

const Endpoint = require('../Endpoint');
const Session = require('services/Session');

class LogoutController extends Endpoint {
  registerRoutes() {
    this.app.get('/logout', Endpoint.checkAuth);
    this.app.get('/logout', LogoutController.get);
  }

  static get(request, response, next) {
    const tokenRemoved = Session.removeToken(response.locals.user.getToken());

    if (!tokenRemoved) return Endpoint.raiseForbidden(request, response);

    Endpoint.success(response, {
      message: 'Successful logged out',
    });

    return next();
  }
}

module.exports = httpServer => (new LogoutController(httpServer));

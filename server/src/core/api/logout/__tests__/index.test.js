const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');

const API = require('core/api');
const Session = require('services/Session');
const User = require('models/User');

const app = express();
app.use(bodyParser.json());

API.setHttpServer(app);

describe('LogoutController', () => {
  let user;

  beforeEach(() => {
    user = new User({
      id: 1,
      name: 'John Doe',
      email: 'john@doe.com',
    });

    Session.getUsers().set('mock_token', user);
    user.setToken('mock_token');
  });

  it('should throw forbidden when user didn\' has a token', (done) => {
    request(app)
      .get('/logout')
      .expect(403)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });

  it('should throw forbidden when user has a invalid token', (done) => {
    request(app)
      .get('/logout')
      .set('Authorization', Math.random())
      .expect(403)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });

  it('should logout', (done) => {
    request(app)
      .get('/logout')
      .set('Authorization', 'Zup mock_token')
      .expect(200)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });
});

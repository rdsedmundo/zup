const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');

const API = require('core/api');

const app = express();
app.use(bodyParser.json());

API.setHttpServer(app);

describe('LoginController', () => {
  it('should return forbidden when credentials are invalid', (done) => {
    const userData = {
      email: 'rdsedmundo@gmail.com',
      password: '123',
    };

    request(app)
      .post('/login')
      .send(userData)
      .expect(403)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });

  it('should authenticate user', (done) => {
    const userData = {
      email: 'rdsedmundo@gmail.com',
      password: '123456',
    };

    request(app)
      .post('/login')
      .send(userData)
      .expect(200)
      .end((err) => {
        if (err) return done.fail(err);

        return done();
      });
  });
});

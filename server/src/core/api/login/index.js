const md5 = require('md5');
const Endpoint = require('../Endpoint');
const MockUsers = require('mocks/users');
const Session = require('services/Session');

class LoginController extends Endpoint {
  registerRoutes() {
    this.app.post('/login', LoginController.post);
  }

  /**
   * @param object data
   * @return object (User) on success, boolean (false) on failure
   */
  static getUser(data) {
    // Simulate database delay
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const userInDatabase =
          MockUsers.find(user => (user.email === data.email && user.password === data.password));

        if (!userInDatabase) return reject();

        return resolve(Session.addUser(userInDatabase));
      }, 2000);
    });
  }

  static async post(request, response, next) {
    try {
      const data = Object.assign({}, request.body);
      data.password = md5(data.password);

      const user = await LoginController.getUser(data);
      Reflect.deleteProperty(user, 'password');

      Endpoint.success(response, {
        message: 'Authenticated successfully',
        data: user,
      });

      next();
    } catch (error) {
      Endpoint.error(response, {
        status: 403,
        message: 'E-mail and/or password incorrect',
      });
    }
  }
}

module.exports = httpServer => (new LoginController(httpServer));

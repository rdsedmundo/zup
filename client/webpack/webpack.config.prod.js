const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const dependencies = require('../package.json').dependencies;

module.exports = require('./webpack.config.base')({
  entry: {
    bundle: './index.js',
    vendor: Object.keys(dependencies),
  },
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js',
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChuncks: 2,
    }),

    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: { warnings: false },
      comments: false,
    }),

    new ExtractTextPlugin('styles.[hash].css'),

    new HtmlWebpackPlugin({
      inject: true,
      minify: {
        caseSensitive: true,
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        html5: true,
        keepClosingSlash: true,
        minifyCSS: true,
        minifyJS: true,
        preserveLineBreaks: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
      },
      template: 'index.html',
    }),

    new CleanWebpackPlugin('./static', {
      root: path.resolve(__dirname, '../'),
      verbose: true,
      dry: false,
      exclude: [],
    }),
  ],
  rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: [
        { loader: 'babel-loader' },
      ],
    },
  ],
});

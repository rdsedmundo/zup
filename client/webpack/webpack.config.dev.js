const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const dependencies = require('../package.json').dependencies;

module.exports = require('./webpack.config.base')({
  entry: {
    bundle: './index.js',
    vendor: Object.keys(dependencies),
  },
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    contentBase: './src',
    hot: true,
    inline: true,
    compress: true,
    host: '0.0.0.0',
    port: 3000,
  },
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },
  performance: { hints: false },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: { warnings: false },
      comments: false,
      mangle: false,
    }),

    new webpack.HotModuleReplacementPlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChuncks: 2,
    }),

    new ExtractTextPlugin('styles.css'),

    new HtmlWebpackPlugin({
      cache: true,
      inject: true,
      minify: {
        minifyCSS: true,
        minifyJS: true,
      },
      showErrors: true,
      template: 'index.html',
    }),
  ],
  rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: [
        { loader: 'babel-loader' },
      ],
    },
  ],
});

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const nodeEnv = process.env.NODE_ENV || 'development';

const paths = {
  src: path.resolve('./src'),
  build: path.resolve('./static'),
};

const cssConfig = {
  test: /\.css$/,
  use: [
    { loader: 'style-loader' },
    {
      loader: 'css-loader',
      options: { modules: true },
    },
  ],
};

const SassConfig = {
  test: /\.scss$/,
  use: [
    {
      loader: ExtractTextPlugin.extract({
        loader: 'css-loader',
        publicPath: paths.build,
      }),
    },
    {
      loader: 'css-loader',
    },
    {
      loader: 'postcss-loader',
      options: {
        plugins: () => {
          return [
            require('autoprefixer'),
          ];
        },
      },
    },
    {
      loader: 'sass-loader',
    },
  ],
};

const resolve = {
  extensions: ['.js'],
  modules: [paths.src, 'node_modules'],
};

const commonPlugins = [
  new webpack.DefinePlugin({
    'process.env': { NODE_ENV: JSON.stringify(nodeEnv) },
  }),
];

function baseConfig(options) {
  const output = Object.assign({
    path: paths.build,
  }, options.output);

  const rules = options.rules.concat([cssConfig, SassConfig]);
  const plugins = options.plugins.concat(commonPlugins);

  return ({
    context: paths.src,
    devtool: options.devtool || '',
    devServer: options.devServer || {},
    entry: options.entry,
    module: { rules },
    performance: { hints: false },
    output,
    plugins,
    resolve,
  });
}

module.exports = baseConfig;

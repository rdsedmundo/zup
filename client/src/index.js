import 'babel-polyfill';
import angular from 'angular';
import 'angular-ui-router';
import 'angular-animate';
import 'angular-loading-bar';
import 'angular-local-storage';

import './index.scss';

(() => {
  const app = angular.module('zup', [
    'ngAnimate',
    'ui.router',
    'angular-loading-bar',
    'LocalStorageModule',
  ]);

  app.constant('API_URL', 'http://localhost:5050');

  app.config(($qProvider, $httpProvider, $stateProvider, $urlRouterProvider) => {
    $qProvider.errorOnUnhandledRejections(false);

    // eslint-disable-next-line
    $httpProvider.defaults.cache = false;
    $httpProvider.interceptors.push('HttpInterceptor');

    $stateProvider.state({
      name: 'login',
      url: '/login',
      templateUrl: 'components/login/index.html',
      controller: 'LoginController as vm',
    });

    $stateProvider.state({
      name: 'dashboard',
      url: '/dashboard',
      templateUrl: 'components/dashboard/index.html',
      controller: 'DashboardController as vm',
    });

    $urlRouterProvider.otherwise('/login');
  });
})();

require('./services');
require('./components');

import angular from 'angular';

(() => {
  angular.module('zup')
    .controller('DashboardController', DashboardController);

  function DashboardController($state, $scope, localStorageService, LoginService, TodoService) {
    const vm = this;

    let todos;

    vm.add = {
      title: '',
      description: '',
    };
    vm.searchInput = '';
    vm.user = {};
    vm.viewFinished = false;

    ////////////////
    vm.addTodo = addTodo;
    vm.deleteTodo = deleteTodo;
    vm.getTodos = getTodos;
    vm.logout = logout;
    vm.toggleEdit = toggleEdit;
    vm.updateTodo = updateTodo;

    ControllerInit();
    ////////////////

    function ControllerInit() {
      if (!localStorageService.get('user')) {
        return $state.go('login');
      }

      vm.user = localStorageService.get('user');

      TodoService.get()
        .then((data) => {
          todos = data.data;
        });
    }

    function addTodo() {
      TodoService.add({ ...vm.add })
        .then((data) => {
          if (data.status === 'success') {
            todos.push(data.data);
          }
        });

      vm.add.title = vm.add.description = '';
    }

    function deleteTodo(id) {
      TodoService.remove(id)
        .then((data) => {
          if (data.status === 'success') {
            const index = todos.indexOf(todos.find(t => t.id === id));

            todos.splice(index, 1);
          }
        });
    }

    function getTodos() {
      if (!todos) return [];
      if (vm.viewFinished) return todos;

      return todos.filter(t => t.finished === false || t.backup);
    }

    function logout() {
      LoginService.logout()
        .finally(() => {
          localStorageService.remove('user');
          $state.go('login');
        });
    }

    function toggleEdit(todo) {
      if (todo.editMode) {
        // eslint-disable-next-line
        todo.title = todo.backup.title;
        // eslint-disable-next-line
        todo.description = todo.backup.description;
        // eslint-disable-next-line
        todo.finished = todo.backup.finished;
        // eslint-disable-next-line
        todo.editMode = false;

        Reflect.deleteProperty(todo, 'backup');
      } else {
        // eslint-disable-next-line
        todo.backup = {
          title: todo.title,
          description: todo.description,
        };
        // eslint-disable-next-line
        todo.editMode = true;
      }
    }

    function updateTodo(todo) {
      TodoService.update(todo)
        .then((data) => {
          if (data.status === 'success') {
            const index = todos.indexOf(todo);

            todos[index] = data.data;
          }
        });
    }
  }
})();

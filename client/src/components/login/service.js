import angular from 'angular';

(() => {
  angular.module('zup')
    .factory('LoginService', LoginService);

  function LoginService(API_URL, $http) {
    const service = {
      authenticate,
      logout,
    };

    return service;

    //////////////////
    function authenticate(email, password) {
      return $http.post(`${API_URL}/login`, { email, password })
        .then(response => response.data);
    }

    function logout() {
      return $http.get(`${API_URL}/logout`)
        .then(response => response.data);
    }
  }
})();

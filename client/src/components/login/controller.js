import angular from 'angular';

(() => {
  angular.module('zup')
    .controller('LoginController', LoginController);

  function LoginController($state, LoginService, localStorageService) {
    const vm = this;

    vm.email = 'rdsedmundo@gmail.com';
    vm.password = '123456';

    ////////////////////
    vm.authenticate = authenticate;

    ControllerInit();
    ////////////////////

    function ControllerInit() {
      if (localStorageService.get('user')) {
        return $state.go('dashboard');
      }
    }

    function authenticate() {
      LoginService
        .authenticate(vm.email, vm.password)
        .then((data) => {
          if (data.status !== 'success') {
            return false;
          }

          localStorageService.set('user', data.data);

          return $state.go('dashboard');
        });
    }
  }
})();

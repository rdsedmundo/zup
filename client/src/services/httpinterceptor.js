import angular from 'angular';

(() => {
  angular.module('zup')
    .factory('HttpInterceptor', HttpInterceptor);

  function HttpInterceptor(API_URL, $injector, $q, $log, localStorageService) {
    let $state;

    const interceptor = {
      request: requestIntercept,
      responseError: responseErrorIntercept,
    };

    return interceptor;

    function requestIntercept(config) {
      const selfConfig = Object.assign({}, config);

      const $canceller = $q.defer();
      selfConfig.timeout = $canceller.promise;

      const targetUrl = config.url;

      const allowedUrls = [
        'components/login/index.html',
        `${API_URL}/login`,
      ];

      if (!localStorageService.get('user') && allowedUrls.indexOf(targetUrl) < 0) {
        $canceller.resolve();

        $state = $state || $injector.get('$state');
        $state.go('login');
      } else {
        $canceller.reject();
      }

      // If the request target is on API, set the auth token header
      if (localStorageService.get('user') && targetUrl.indexOf(API_URL) >= 0) {
        selfConfig.headers.Authorization = `Zup ${localStorageService.get('user').token}`;
      }

      return selfConfig;
    }

    function responseErrorIntercept(response) {
      $state = $state || $injector.get('$state');

      if (response.status === 403) {
        localStorageService.remove('user');
        $state.go('login');
      }

      return $q.reject(response);
    }
  }
})();

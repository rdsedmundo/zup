import angular from 'angular';

(() => {
  angular.module('zup')
    .factory('TodoService', TodoService);

  function TodoService(API_URL, $http) {
    const service = {
      add,
      get,
      remove,
      update,
    };

    return service;

    function add(payload) {
      return $http.post(`${API_URL}/todo`, payload)
        .then(response => response.data);
    }

    function get() {
      return $http.get(`${API_URL}/todo`)
        .then(response => response.data);
    }

    function remove(id) {
      return $http.delete(`${API_URL}/todo?id=${id}`)
        .then(response => response.data);
    }

    function update(payload) {
      const data = { ...payload };
      Reflect.deleteProperty(data, 'backup');
      Reflect.deleteProperty(data, 'editMode');

      return $http.put(`${API_URL}/todo`, data)
        .then(response => response.data);
    }
  }
})();
